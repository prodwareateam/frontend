(function () {
    'use strict';

    /* JAVASCRIPT */

    /**
     * Sample Object/function
     */
    function Sample () {

        /***************** PRIVATE *******************/

        /**
         * Directives link function
         */
        function _link(scope, iElem, iAttrs, controllers) {
            var s = scope;
            //scope.vm.haim = "yossi";
            //scope.vm.onDone()('hello');
        }
        
        // function SampleController($scope, $element){
        //     var vm = this;
        //     var s = $scope;
        // }

        /****************** PUBLIC *******************/
        var directive = {
            restrict: 'E',
            replace: true,
            scope: {},
            // scope: {
            //     data: '@',
            //     haim: '=',
            //     onDone: '&'
            // },
            templateUrl: 'common/directives/sample/sample.directive.html',
            link: _link//,
            // controller: SampleController,
            // controllerAs: 'vm',
            // bindToController: true
        };

        return directive;

    }

    /* ANGULAR */
    angular
        .module('common')
        .directive('sample', Sample );

})();
