(function () {
    'use strict';

    /* JAVASCRIPT */

    /**
     * HomeController Object/function
     */
    function HomeController(homeService) {

        // vm (view-model) is the object we bind to (this controller).
        var vm = this;

        /***************** PRIVATE *******************/
        var _name = 'HomeController';

        /**
         * getName() - Private function
         */
        function _getName(val) {
            return _name;
        }
        
        function _setName(val){
            _name = val;
        }

        /****************** PUBLIC *******************/
        vm.getName = _getName;
        vm.setName = _setName;
        vm.service = homeService;
        vm.name = _name;

    }

    angular
        .module('home')
        .controller('homeController', HomeController);

})();
